/**
 * Grunt config file
 */
module.exports = function (grunt) {
    grunt.initConfig({
        jshint: {
            files: ['scripts/**/*.js', "!scripts/core/**/*.js", '!scripts/templates.js', "!scripts/build/*.js"],
            options: {
                globals: {
                    window: true,
                    asi: false
                }
            }
        },
        concat: {
            options: {
                separator: '\n',
            },
            libs: {
                src: ['app/vendors/handlebars.min.js'],
                dest: 'scripts/handlebars.min.js',
            },
            liteJS: {
                src: ['app/liteJS/liteJS.js',
                    'app/liteJS/liteJS-ajax.js',
                    'app/liteJS/liteJS-query.js',
                    'app/liteJS/liteJS-promise.js',
                    'app/liteJS/liteJS-router.js'
                ],
                dest: 'scripts/liteJS.js',
            },
            appJS: {
                src: [
                    'scripts/templates.js',
                    'app/components/**/*.js'
                ],
                dest: 'scripts/app.js'
            }
        },
        uglify: {
            options: {
                preserveComments: false,
                mangle: true,
                compress: true,
                sourceMap: false,
                compress: {
                    drop_console: true
                },
                beautify: {
                    beautify: false
                }
            },
            js: {
                files: {
                    'scripts/liteJS.js': [
                        'app/liteJS/liteJS.js',
                        'app/liteJS/liteJS-ajax.js',
                        'app/liteJS/liteJS-query.js',
                        'app/liteJS/liteJS-promise.js',
                        'app/liteJS/liteJS-router.js'],
                    'scripts/app.js': [
                        'app/components/**/*.js',
                        'scripts/templates.js'
                    ],
                    'scripts/handlebars.min.js': [
                        'app/vendors/handlebars.min.js'
                    ]
                }
            }
        },
        clean: {
            build: [
                'scripts/*.js',
                'scripts/build'
            ],
        },
        watch: {
            scripts: {
                files: ['app/**/*.js'],
                tasks: ['concat']
            },
            templates: {
                files: ['app/**/*.html'],
                tasks: ['handlebars', 'concat']
            }
        },
        handlebars: {
            compile: {
                options: {
                    namespace: 'templates',
                    processName: function (filePath) {
                        var pieces = filePath.split('/');
                        return pieces[pieces.length - 1].split(".")[0];
                    }
                },
                files: {
                    'scripts/templates.js': ['app/**/*.html']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-handlebars');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['clean', 'handlebars', 'concat', 'watch']);
    grunt.registerTask('build-prod', ['clean', 'handlebars', 'uglify']);
};