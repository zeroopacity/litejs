(function () {
    var router = liteJS.module("ltRouter");
    router.provider("router", ["query", "config", "instanceFactory"], function (query, configService, instanceFactory) {
        var routes = [],
            defaultRoute,
            self = this;

        function registerRoutes(_routes) {
            for (var key in _routes) {
                if (key === "/") {
                    defaultRoute = {
                        path: key,
                        routeObj: _routes[key]
                    };
                } else {
                    routes.push({
                        path: key.split("?")[0],
                        routeObj: _routes[key]
                    });
                }
            }
        }
        function changeRoute(url) {
            if (self["before_route_change"]) {
                var routeConfig = getRouteInfo(url);
                routeConfig.done = function (obj) {
                    query(document).trigger("change_app_route", {
                        routeConfig: getRouteInfo(url),
                        params: query.queryParam(window.location.hash),
                        done: function () {
                            self.trigger("after_route_change", routeConfig);
                        }
                    });
                }
                self.trigger("before_route_change", routeConfig);
            } else {
                query(document).trigger("change_app_route", {
                    routeConfig: getRouteInfo(url),
                    params: query.queryParam(window.location.hash),
                    done: function () {
                        self.trigger("after_route_change", routeConfig);
                    }
                });
            }
        }
        function goTo(url, hasChanged) {
            if (hasChanged) {
                changeRoute(url);
            } else {
                window.location.hash = url;
            }
        }
        function getRouteInfo(url) {
            var routeName,
                routeObj;
            if (url) {
                routeName = url.split("?")[0];
            }
            for (var i = 0; i < routes.length; i++) {
                if (routeName === routes[i].path) {
                    routeObj = routes[i].routeObj;
                    break;
                }
            }
            if (!routeObj) {
                routeObj = defaultRoute.routeObj;
            }
            return routeObj;
        }
        function bindRouteChangeEvent() {
            var cachedView,
                cachedLayout,
                ltView;
            query(document).on("change_app_route", function (e) {
                var obj = e.data;
                var routeConfig = obj.routeConfig;
                if (!cachedView) {
                    cachedView = query.new("div", true);
                    query(configService.hostSelector).append(cachedView);
                }
                cachedLayout = cachedView.attr("layout");

                var jsonData = JSON.stringify(e.data.params);
                if (!cachedLayout) {
                    cachedView.html(configService.templateFunc(routeConfig.layout || configService.defaultLayout));
                    cachedView.attr("layout", routeConfig.layout || configService.defaultLayout);

                    ltView = cachedView.find("[ltView]").html("<div data-component='" + routeConfig.component + "' data-init='" + jsonData + "'></div>");
                    instanceFactory.initUIComponents(cachedView.find("[data-component]"));
                } else {
                    var layoutChanged;
                    if (routeConfig.layout && cachedLayout !== routeConfig.layout) {
                        cachedView.html(configService.templateFunc(routeConfig.layout));
                        cachedView.attr("layout", routeConfig.layout);
                        layoutChanged = true;
                    }
                    if (!routeConfig.layout && cachedLayout !== configService.defaultLayout) {
                        cachedView.html(configService.templateFunc(configService.defaultLayout));
                        cachedView.attr("layout", configService.defaultLayout);
                        layoutChanged = true;
                    }

                    ltView = cachedView.find("[ltView]");
                    var component = query.new("div", true);
                    component.attr("data-component", routeConfig.component);
                    component.attr("data-init", jsonData);
                    component.addClass((routeConfig.animation || configService.defaultAnimation) || "");
                    ltView.html(component);

                    instanceFactory.initUIComponents(layoutChanged ? cachedView.find("[data-component]") : component);
                }

                query(document).trigger("route_changed", cachedView);
                e.data.done();
            });
        };
        this.export = {
            registerRoutes: function (routes) {
                registerRoutes(routes);
            },
            beforeRouteChange: function (callback) {
                self.on("before_route_change", callback, self);
            },
            afterRouteChange: function (callback) {
                self.on("after_route_change", callback, self);
            },
            lt$get: {
                goTo: function (url) {
                    goTo(url);
                }
            }
        };

        router.ready(function () {
            instanceFactory.initUIComponents(query(configService.hostSelector).find("[data-component]"));

            bindRouteChangeEvent();
            window.addEventListener('hashchange', function () {
                goTo(location.hash.slice(1), true);
            });
            goTo(location.hash.slice(1), true);
        });

    });
}());