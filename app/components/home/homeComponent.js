app.component("homeComponent", ["ajax", "q"], function (ajax, q) {
    var self = this;
    self.init = function (data) {
        self.render("homeTemplate", { Name: "Hello, Vijay" });
        var str = "";
        var startTime = new Date().getTime();
        for (var i = 0; i < 1000; i++) {
            //var data = JSON.stringify({ count: i });
            //self.renderHtmlString("<dashboard-item-component data-init=" + data + "></dashboard-item-component>", true);
            str += "<div data-component='dashboardItemComponent' data-init='{}'></div>", true;
        }
        self.addHtmlString(str);
        var end1 = new Date().getTime();
        console.log("append html = ", end1 - startTime);
        self.initChildrenComponent();
        console.log("init Component = ", new Date().getTime() - startTime);

    };
    self.destroy = function () {
    };
});