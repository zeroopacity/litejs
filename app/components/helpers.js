liteJS.module("helpers")
    .component("topNotificationComponent", function () {
        var self = this;
        self.init = function () {
            self.addHtmlString("<span>No Notification..</span>");
        };
    }, true)
    .service("alertService", function () {
        this.export = {
            showConfirmBox: function (msg) {
                return confirm(msg);
            }
        }
    });